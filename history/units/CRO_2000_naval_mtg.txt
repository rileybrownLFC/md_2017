units = {
    fleet = {
        name = "Navy Flotilla"
        naval_base = 3924 #Split
        task_force = {
            name = "Surface Action Squadron"
			location = 3924
            ship = { name = "Kralj Petar Krešimir IV" definition = corvette start_experience_factor = 0.25 equipment = { corvette_hull_3 = { amount = 1 owner = CRO version_name = "Kralj Class" } } }
            ship = { name = "Šibenik" definition = corvette start_experience_factor = 0.25 equipment = { corvette_hull_2 = { amount = 1 owner = SER version_name = "Končar Class" } } }
		
        }
    }
}