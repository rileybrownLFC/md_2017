units = {

	### Naval OOB ###
	fleet = {
		name = "Mornarica Vojske Crne Gore"
		naval_base = 9809
		task_force = {
			name = "Površinske pomorske snage"
			location = 9809
			ship = { name = "Kotor" definition = corvette start_experience_factor = 0.25 equipment = { missile_corvette_1 = { amount = 1 owner = SER } } }
			ship = { name = "Novi Sad" definition = corvette start_experience_factor = 0.25 equipment = { missile_corvette_1 = { amount = 1 owner = SER } } }
		}
	}
}
